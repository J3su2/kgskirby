<div class="jumbotron" style="background-image: url('<?= $kirby->url('assets') ?>/img/bg3.jpg'); background-size: cover;">
  <div class="container">
    <h1 class="title"><?= $page->title() ?>
      <small class="text-muted"><?= $page->subtitle() ?></small>
    </h1>
  </div>
</div>

<!-- Ab hier der richtige Inhalt, der auf jeder Seite individuell sein kann -->

<div class="main main-raised">
  <div class="container">
    <div class="section text-left">