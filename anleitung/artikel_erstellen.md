# Kopieren alter Artikel

* Man geht auf niehaus-ol.de/panel
* Man loggt sich mit den bekanntgegebenen Zugangsdaten ein
* Man klickt auf "Über die Fächer" und kommt damit zu http://kirby/panel/pages/blogs
* "Hinzufügen" (oben rechts) und diesen Dialog ausfüllen:

![Kirby läuft](neu_anlegen.png)


* Es erscheint eine Maske, die man hoffentlich intuitiv benutzen kann
* Wichtig ist, rechts das "Tag" einzustellen. Das ist "Biologie" für das Fach Biologie und so weiter
* Für die Fächer sind die Tags schon voreingestellt, da nimmt das, was nach den ersten paar Buchstaben vorgeschlagen wird
* Die Artikel werden nach Datum sortiert, dafür bitte ein Datum einstellen, zur Not raten. 
* In dem Feld **Inhalt** will man in 99% der Fälle **"Text"** haben
* Im Text kann man dann (links klicken) natürlich auf Überschriften, **fett**, *kursiv* und so weiter einstellen.

> Bitte *immer* Bilder manuell hochladen und im Text auf diese verweisen. *Kein Copy&Paste von Bildern in den Text*, das führt später zu Problemen.
> Jedes Bild ist also auch unter dem Textabschnitt in der Gallerie zu sehen, sonst ist was falsch!

![Kirby läuft](anfang.png)


* Am Ende sollte der Artikel sinngemäß so aussehen, wie im das nächste Bild zeigt.
* Ich habe noch 2 Bilder hochgeladen (die tauchen automatisch unter dem Artikel auf, man kann *optional* ein Karousel einstellen.)


![Kirby läuft](fertig.png)

* ganz am Ende muss der Artikel noch veröffentlicht werden, das sind 2 Mausklicks.
* wenn man das nicht macht wird der Artikel versteckt, bis man es nachholt. So kann man z.B. für Projektteage schon Artikel verfassen, die noch nicht zu sehen sein sollen. Am Stichtag macht man die 2 Klicks und alles wird sichtbar.


![Kirby läuft](veroeffentlichen.png)

