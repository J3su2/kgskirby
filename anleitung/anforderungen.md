                   
# *Anwendungsfälle*

## SchülerInnen
- [ ] Stundenplan einsehen
- [ ] Termine Klasse/Kurs
  - [ ] Das bildet Webuntis ab, zusammen mit dem Messenger. Können wir uns eventuell sparen. Weniger ist mehr.
     
## LehrerInnen (nach Login)
- [ ] FAQ zu
  - [ ] Datenschutz    
  - [ ] Abitur 
  
## Zugriff Dateien
- [ ] ABI
- [ ] Abschlussarbeiten    
- [ ] Formulare aller Art
- [ ] Verschicken der Voucher 
  - [JS Plugin](https://statamic.com/forum/4165-javascript-php-plugin)
  - [Event-Listener](https://docs.statamic.com/addons/classes/event-listeners)
- [ ] Eltern (ohne Login???)
- [ ] Lehrmittelausleihe/Bücherlisten
- [ ] PDFs
- [ ] Einkaufskorb     bei Thiemann     
    
## Kollegiumsinfo
- [ ] Termine Klasse/Kurs
- [ ] Anmeldung 5
- [ ] Schulwechsel
- [ ] Krankmeldung
- [ ] Essen     
  - [ ] ab-/bestellen
- [ ] Lehrpläne/Netzpläne

# Externe
- [ ] Anfahrt Schule
  - [ ] https://statamic.com/marketplace/addons/google-maps 



## Alle
- [ ] Informationen finden
- [ ] Artikel lesen
- [ ] Kontakt Sekri
- [ ] Kontakt Schulleitung
- [ ] Konzepte
- [ ] Termine der Schule

## Anforderungen
### Öffentlicher und geschützter Bereich
- [ ] Login-Mechanismus    
  - [ ] https://docs.statamic.com/permissions 
- [ ] Sehr gute Durchsuchbarkeit der Seite
  - [ ] https://docs.statamic.com/search 
- [ ] Ical o.ä. Einbettung
  - [ ] https://statamic.com/marketplace/addons/ical/docs 
  - [ ] https://github.com/edalzell/statamic-ical/issues 
  - [ ] https://statamic.com/marketplace/addons/happy-dates
  - [ ] Kostet 40 Euro
- [ ] Kollektion Kollegium mit Rollen als Export aus DB
- [ ] Ebenso Login SchülerInnen (Verwendung Hash?)
- [ ] Müssen Schüler (oder Eltern) wirklich einen Login haben? wozu?
- [ ] Import der Inhalte aus altem CMS muss möglich sein
  - [ ] Idealerweise ja, aber sonst:
    - [ ] Alle Fachbereichsleiter und ein paar Freiwillige setzen sich 2 Stunden dran und fertig. Man braucht nicht lange für das manuelle kopieren. Wenn man das vorbereitet sollte das gehen.
    - [ ] Das waren dann sowas wie 16 Personen-Stunden...


## Konsequenzen
- [ ] Entrümpelung der “alten” HP
- [ ] Doppelte Pflege von L und S Daten unbedingt zu vermeiden. Ggf. per Skript Erzeugung der notwendigen Dateien
